@extends('layouts.default')

@section('content')

    <div class="flex justify-center bg-grey-lighter">
        <div class="w-full max-w-sm">

            <p class="text-2xl font-bold">Weather Search Tool</p>
            <weathersearch></weathersearch>

            <p class="text-center text-gray-500 text-xs pt-4">
                &copy;2019 Donovan Sargeant.
            </p>
        </div>
    </div>

@endsection