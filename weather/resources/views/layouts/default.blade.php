<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ env('APP_NAME') }}</title>

    <script>window.Laravel = {csrfToken: '{{ csrf_token() }}'}</script>

    <link rel="icon" href="https://bevie.co/wp-content/uploads/2018/01/cropped-Favicon_Leaf_PNG-32x32.png" sizes="32x32">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

</head>
<body class="bg-gray-100 h-screen antialiased leading-none">
<div id="app">

    <nav class="bg-blue-900 shadow mb-8 py-6">
        <div class="container mx-auto px-6 md:px-0">
            <div class="flex items-center justify-center">
                <div class="mr-6">
                    <a href="{{ url('/') }}" class="text-lg font-semibold text-gray-100 no-underline">{{ env('APP_NAME') }}</a>
                </div>
                <div class="flex-1 text-right">
                </div>
            </div>
        </div>
    </nav>

    <div class="container mx-auto">
        @yield('content')
    </div>
</div>

<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>

</body>
</html>
