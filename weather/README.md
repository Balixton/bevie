# Bevie Weather API Test

## Installation
``` bash
git clone git clone https://Balixton@bitbucket.org/Balixton/bevie.git bevie-weather

cd bevie-weather/weather

composer install

```
- I ran this project on Ubuntu 14.04, on PHP 7

## API Comments
- I am aware there is a limitation to how many requests are allowed on the free version of the weather API. I have assumed that in production we would have a paid subscription. So there would be no need to cache data locally to try reduce the number of API calls made.
- The API from [https://openweathermap.org], does not offer a 7 day forecast under it's free subscription. So I have used the free 5 day / 3 hour forecast option.
    - The problem with this option is it gives you the weather every 3 hours over the 5 day period. 
    - For the sake of this exercise I have just taken the weather at 12:00 each day, and used that values.
- Icons are referenced from the API directly.     
    
## Testing
- In the root directory of the project run ./vendor/bin/phpunit
