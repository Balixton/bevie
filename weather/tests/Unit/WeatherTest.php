<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Weather;

class WeatherTest extends TestCase {

    protected $sample_weather_data = '{"coord":{"lon":-0.13,"lat":51.51},"weather":[{"id":300,"main":"Drizzle","description":"light intensity drizzle","icon":"09d"}],"base":"stations","main":{"temp":280.32,"pressure":1012,"humidity":81,"temp_min":279.15,"temp_max":281.15},"visibility":10000,"wind":{"speed":4.1,"deg":80},"clouds":{"all":90},"dt":1485789600,"sys":{"type":1,"id":5091,"message":0.0103,"country":"GB","sunrise":1485762037,"sunset":1485794875},"id":2643743,"name":"London","cod":200}';

    public function testMethod_extractWeatherData() {
        $Weather = new Weather();

        $sample_weather_data = json_decode($this->sample_weather_data,true);

        $this->assertIsArray(
            $Weather->extractWeatherData($sample_weather_data),
            'An array of weather data is expected'
        );

        // We expect the cleaned data to have certain key pieces of data returned
        $clean_data = $Weather->extractWeatherData($sample_weather_data);
        $this->assertArrayHasKey('dt',$clean_data);
        $this->assertArrayHasKey('icon',$clean_data);
        $this->assertArrayHasKey('description',$clean_data);
        $this->assertArrayHasKey('temp',$clean_data);
        $this->assertArrayHasKey('wind',$clean_data);
        $this->assertArrayHasKey('clouds',$clean_data);
        $this->assertArrayHasKey('temp_min',$clean_data);
        $this->assertArrayHasKey('temp_max',$clean_data);
        $this->assertArrayHasKey('pressure',$clean_data);
        $this->assertArrayHasKey('humidity',$clean_data);
    }

    public function testMethod_formatDescription() {
        $Weather = new Weather();

        $this->assertEquals('', $Weather->formatDescription(''), 'Passing no data in should result in an empty string.');

        $this->assertEquals('Drizzle, light intensity drizzle', $Weather->formatDescription(json_decode($this->sample_weather_data,true)), 'The method did not format the string correctly.');
    }

    public function testExternalAPIEndpoint() {
        $Weather = new Weather();

        $this->assertFalse($Weather->getDataFromAPI('query','endpoint'),'Passing an invalid API endpoint should fail.');

        // This isn't a proper unit test as we shouldn't be using unit testing to ensure a 3rd party provider is functioning, but this does ensure the method is sending back a string of data.
        $this->assertIsString($Weather->getDataFromAPI('q=london'),'We should receive a valid string from the API provider');
    }

}