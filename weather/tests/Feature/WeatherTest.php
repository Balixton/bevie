<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class WeatherTest extends TestCase
{
    public function testBasicTest() {
        $response = $this->get('/');
        $response->assertStatus(200);
    }

    public function testInternalAPIEndpoint() {
        $response = $this->get('/api/citysearch?cityname=london');
        $response->assertStatus(200)->assertJson([['name' => 'London']]);
    }

}