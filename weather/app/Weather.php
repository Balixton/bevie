<?php

namespace App;

class Weather {

    // For this exercise I've just set the value here. Usually you'd have it in your config files as you'd usually have different keys for dev and live environments
    const API_KEY = '31f1dd42e907827c9d181b6fab2f7829';

    const URL = 'https://api.openweathermap.org/data/2.5/';
    const ICON_URL = 'http://openweathermap.org/img/w/';

    protected $validEnpoints = ['weather','forecast',"group"];

    public function __construct() {
    }

    /***
     * @param string $cityname, the name of the city being searched
     * @param bool $fetchForecastData, if you do not want the weather forecast set to false
     * @return array|bool, will return false if no data was returned or there was a error
     */
    public function getWeatherByCityName($cityname='',$fetchForecastData=true) {
        $results = false;

        // Let us go get the current weather
        if (($weather = $this->getDataFromAPI("q={$cityname}")) !== false) {
            // We got raw data from the API, let us clean it up and get what we want to return
            $data = json_decode($weather,true);
            $current_weather = [];

            // Let us check if we have multiple locations
            if (isset($data['list'])) {
                foreach($data['list'] as $city) {
                    if ( ($clean_data = $this->processCurrentWeather($city)) !== false) {
                        $current_weather[] = $clean_data;
                    }
                }
            } else {
                if ( ($clean_data = $this->processCurrentWeather($data)) !== false) {
                    $current_weather[] = $clean_data;
                }
            }


            if ($fetchForecastData) {
                foreach($current_weather as $idx => $weather) {
                    // We need to fetch the forecast data
                    if (($forecast = $this->getDataFromAPI("id={$weather['id']}","forecast")) !== false) {
                        if ( ($forecast_data = $this->processForecastWeather(json_decode($forecast,true))) !== false) {
                            $current_weather[$idx]['forecasts'] = $forecast_data;
                        }
                    }
                }
            }

            $results = $current_weather;
        }

        return $results;
    }

    private function processCurrentWeather($data=null) {
        if (isset($data['id'],$data['name'])) {
            $result = array_merge(['id'=>$data['id'],'name'=>$data['name']],$this->extractWeatherData($data));
        } else
            $result = false;

        return $result;
    }

    private function processForecastWeather($data=null) {
        $result = false;
        if (isset($data['list'])) {
            foreach($data['list'] as $forecast) {
                if ($this->checkWeatherDate($forecast))
                    $result[] = $this->extractWeatherData($forecast);
            }
        }

        return $result;
    }

    /***
     * @param string $cityname, the name of the city being search
     * @param string $endpoint, from where we will get the data from
     * @return string|bool, will return false if no data was returned or there was a error. Else it will return a string of the raw data from the API
     */
    public function getDataFromAPI($query_tring='',$endpoint='weather') {
        $results = false;

        if (in_array($endpoint,$this->validEnpoints)) {

            $request_url = self::URL."{$endpoint}?{$query_tring}&units=metric&appid=".self::API_KEY;

            try {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $request_url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_TIMEOUT, 5);
                $curl_result = curl_exec($ch);
                if (FALSE !== $curl_result) {
                    $retcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    if ($retcode >= 200 && $retcode <= 299) {
                        // Got a good http response, now let check the data
                        if (is_array(json_decode($curl_result,true))) {
                            $results = $curl_result; // We have good data returned so send it back
                        }
                    }
                }
                curl_close($ch);
            } catch (Exception $e) {
                // Failed, we could log this locally, and/or trigger system alerts to notify us if required. Excluded fot this exercise
            }
        }

        return $results;
    }

    /***
     * @param $data, a single array of city weather data
     * @return array|bool, Will return false if we have invalid data, else it will return an array of weather data
     */
    public function extractWeatherData($data=null) {

        $result = [
            'dt' => $this->formatDate($data),
            'dt_day' => $this->formatDate($data,'D'),
            'icon' => (isset($data['weather'][0]['icon'])) ? self::ICON_URL."{$data['weather'][0]['icon']}.png" : '',
            'description' => $this->formatDescription($data),
            'temp' => (isset($data['main']['temp'])) ? round($data['main']['temp'],0) : '',
            'wind' => (isset($data['wind']['speed'])) ? $data['wind']['speed'].' m/s' : '',
            'clouds' => (isset($data['clouds']['all'])) ? $data['clouds']['all'].' %' : '',
            'temp_min' => (isset($data['main']['temp_min'])) ? round($data['main']['temp_min'],0) : '',
            'temp_max' => (isset($data['main']['temp_max'])) ? round($data['main']['temp_max'],0) : '',
            'pressure' => (isset($data['main']['pressure'])) ? $data['main']['pressure'].' hpa' : '',
            'humidity' => (isset($data['main']['humidity'])) ? $data['main']['humidity'].' %' : '',
        ];

        return $result;
    }

    public function formatDescription($data) {
        // Let us put a nice description together
        $description = (isset($data['weather'][0]['main'])) ? "{$data['weather'][0]['main']}" : '';

        if (isset($data['weather'][0]['id'])) {
            $id = $data['weather'][0]['id'];

            if ($id >= 700 && $id <= 799) { // Atmosphere
                // I don't like these descriptions so will not use them
            } else {
                if (isset($data['weather'][0]['description'])) {
                    $description .= ', '.$data['weather'][0]['description'];

                    if ($id >= 500 && $id <= 599) { // Rain
                        if (isset($data['rain']['1h']))
                            $description .= ' ('.$data['rain']['1h'].' mm last hour)';
                        if (isset($weather['rain']['3h']))
                            $description .= ' ('.$data['rain']['3h'].' mm last 3 hours)';
                    }

                    if ($id >= 500 && $id <= 599) { // Snow
                        if (isset($data['snow']['1h']))
                            $description .= ' ('.$data['snow']['1h'].' mm last hour)';
                        if (isset($weather['snow']['3h']))
                            $description .= ' ('.$data['snow']['3h'].' mm last 3 hours)';
                    }
                }
            }
        }

        return $description;
    }

    private function formatDate($data,$custom_format='D, d M Y H:i') {
        $dt = '';
        if (isset($data['dt'])) {
            $date = new \DateTime('@'.$data['dt']);
            $dt = $date->format($custom_format);
        }

        return $dt;
    }

    private function checkWeatherDate($data) {
        $result = false;

        $weather_time = new \DateTime('@'.$data['dt']);
        $weather_time = $weather_time->format('H');
        if ($weather_time == '12')
            $result = true;

        return $result;
    }
}