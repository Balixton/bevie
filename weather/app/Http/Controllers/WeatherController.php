<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Weather;

class WeatherController extends Controller
{
    public function index() {
        return view('weather/index');
    }

    public function citysearch(Request $request) {
        $Weather = new Weather();

        // Let us init our return data
        $data = [];

        $cityname = $request->query('cityname','');
        if ($cityname != '') {
            if ( ($results = $Weather->getWeatherByCityName($cityname)) !== false)
                $data = $results;
        }

        return $data;
    }
}
