<?php


namespace src;

class RobotClass {

    const xMIN = 0;
    const yMIN = 0;
    const xMAX = 5;
    const yMAX = 5;

    protected $currentPosition = [
        'x'=> 0,
        'y' => 0
    ];

    protected $currentFacing;

    protected $robotStarted = false; // This will be set once we receive a valid PLACE instruction

    protected $tableDimensions = [
        'xMin' => self::xMIN,
        'yMin' => self::yMIN,
        'xMax' => self::xMAX,
        'yMax' => self::yMAX,
    ];

    protected $movements = 1; // How many movements forward the robot can make. Brief only mentioned 1 block at a time, but including this means we can move than 1 block if required in future

    protected $validInstructions = ['PLACE','MOVE','LEFT','RIGHT','REPORT'];

    // I've setup the variable like this so you can add other directions like North East, South West, etc. The order of the array elements is also important as this affects the directions beinffaced when turning
    protected $facingParams = [
        'NORTH' => ['x' => 0, 'y' => 1],
        'EAST' => ['x' => 1, 'y' => 0],
        'SOUTH' => ['x' => 0, 'y' => -1],
        'WEST' => ['x' => -1, 'y' => 0],
    ];

    // Initialise the table
    public function __construct($tableDimenstions = null)
    {
        // I allow the person to be configure their own table dimensions. I have not added any validations to ensure the dimensions are valid. Eg; the max value is greater than the min value, etc
        if (!is_null($tableDimenstions))
            array_merge($this->tableDimensions,$tableDimenstions);
    }

    public function setPosition($x=self::xMIN, $y=self::yMIN) {
        $this->currentPosition = [
            'x' => $x,
            'y' => $y,
        ];
    }

    public function getPosition() {
        return $this->currentPosition;
    }

    public function setFacing($facing=null) {
        if (!is_null($facing))
            $this->currentFacing = $facing;
    }

    public function getFacing() {
        return $this->currentFacing;
    }

    /***
     * @param string $instruction, this will be a single instruction that needs to be processed.
     * @return bool|string, will return false if no instruction was run, else the string value of the instruction processed.
     */
    public function processInstruction($instruction='') {
        $processRun = false; // We will assume the process failed to run unless all validation passes and the instruction is run.

        // We won't assume an instruction is clean so lets do some basic cleaning up
        $instruction = $this->cleanInstruction($instruction);

        if ( ($command = $this->isInstructionValid($instruction)) !== false) {
            $processRun = $instruction;

            // Ok so we have a valid instruction. Lets make sure our robot has been given an initial position to start
            if ($this->robotStarted || $command == 'PLACE') {
                // Now let us decide what to do
                switch ($command) {
                    case 'PLACE':
                        // Let us make sure we've been given valid co-ordinates and facing
                        if (($coords = $this->isPlacementValid($instruction)) !== false) {
                            $this->setPosition($coords[0], $coords[1]);
                            $this->setFacing($coords[2]);
                            $this->robotStarted = true;
                        } else
                            $processRun .= ' ... invalid placement parameters.';

                        break;

                    case 'MOVE':
                        // Ok, let us move forward in the direction we facing, but before we do let us make sure it's safe
                        if (($newPosition = $this->isNextMoveSafe()) !== false)
                            $this->setPosition($newPosition['x'], $newPosition['y']);
                        else
                            $processRun .= ' ... failed, boundary reached.';

                        break;

                    case 'RIGHT':
                        $this->turnRobot();
                        break;

                    case 'LEFT':
                        $this->turnRobot(-1);
                        break;

                    case 'REPORT':
                        // Return the current posistion of the robot
                        $processRun .= PHP_EOL . "Output: {$this->getPosition()['x']},{$this->getPosition()['y']},{$this->getFacing()}" . PHP_EOL;
                        break;
                }
            } else {
                $processRun .= ' ... ignored, robot needs to be placed first.';
            }

        }

        return $processRun;
    }

    private function cleanInstruction($instruction='') {
        return strtoupper(trim($instruction));
    }

    /***
     * @param string $instruction, this will accept an instrction and confirm that it's a valid instruction
     * @return bool|string, false if not valid, else the string value of the instruction
     */
    public function isInstructionValid($instruction='') {
        return (in_array(explode(" ", $instruction)[0], $this->validInstructions)) ? explode(" ", $instruction)[0] : false;
    }

    /***
     * @param string $instruction, will be the full instruction including the position and facing details
     * @return array|bool, will return false if the placement details are not valid, else it will return an array of the placement details [x,y,facing]
     */
    public function isPlacementValid($instruction='') {
        $valid = false; // We will assume the placement details are invalid unless all checks pass

        // Let us expand our instruction to get our placement details
        $details = explode(" ", $instruction);
        if (isset($details[1])) {
            // Let us expand our co-ordinate details
            $coords = explode(",", $details[1]);
            if (count($coords) == 3) {
                if ($this->isCoordsValid($coords[0],$coords[1]) && $this->isFacingValid($coords[2])) {
                    $valid = $coords;
                }
            }
        }

        return $valid;
    }

    /***
     * @param null $x
     * @param null $y
     * @return bool
     *
     * This takes in the x,y co-ordinates and validates that it's within the boundaries of the table.
     * Returns false if the validation fails, true if it passes
     */
    public function isCoordsValid($x=null,$y=null) {
        return ( (!is_null($x) && !is_null($y)) && ($x >= $this->tableDimensions['xMin'] && $x < $this->tableDimensions['xMax']) && ($y >= $this->tableDimensions['yMin'] && $y < $this->tableDimensions['yMax']) ) ? true : false;
    }

    /***
     * @param string $facing
     * @return bool
     *
     * This validates that the facing direction given in the instructions is valid
     */
    public function isFacingValid($facing='') {
        return (in_array($facing, array_keys($this->facingParams))) ? true : false;
    }

    /***
     * @return array|bool, will return false if the next move is not valid, otherwise it will return the new co-ordinates of the robot
     *
     * This will verify if the next move forward is valid.
     */
    private function isNextMoveSafe() {
        $valid = false; // We will assume the next move is not valid unless all checks are passed

        // Calculate our next position, and validate it
        $newPosition = [
            'x' => $this->currentPosition['x'] + ($this->facingParams[$this->currentFacing]['x'] * $this->movements),
            'y' => $this->currentPosition['y'] + ($this->facingParams[$this->currentFacing]['y'] * $this->movements),
        ];
        if ($this->isCoordsValid($newPosition['x'],$newPosition['y']))
            $valid = $newPosition;

        return $valid;
    }

    private function turnRobot($direction=1) {

        $newDirectionKey = array_flip(array_keys($this->facingParams))[$this->currentFacing] + $direction;

        if ($newDirectionKey >= count($this->facingParams))
            $newDirectionKey = 0;
        elseif ($newDirectionKey < 0)
            $newDirectionKey = count($this->facingParams) - 1;

        $this->setFacing(array_keys($this->facingParams)[$newDirectionKey]);
    }
}