<?php

include('src/RobotClass.php');

use PHPUnit\Framework\TestCase;

Class RobotTest extends TestCase {

    protected $Robot;

    public function setUp() {
        $this->Robot = new \src\RobotClass();
    }

    public function testMethod_isCoordsValid() {

        $this->assertInternalType(
            'boolean',
            $this->Robot->isCoordsValid(),
            'The expected return from this method should be of type array'
        );

        $this->assertFalse(
            $this->Robot->isCoordsValid(),
            'No values were passed to the method and it did not return false'
        );

        $this->assertFalse(
            $this->Robot->isCoordsValid(99,99),
            'The values passed were outside the boundaries of the table and it did not return false'
        );

        $this->assertTrue(
            $this->Robot->isCoordsValid(0,0),
            'The values passed were within the boundaries of the table and it did not return true'
        );
    }

    public function testMethod_isFacingValid() {

        $this->assertInternalType(
            'boolean',
            $this->Robot->isFacingValid(),
            'The expected return from this method should be of type array'
        );

        $this->assertFalse(
            $this->Robot->isFacingValid('WRONG'),
            'An invalid facing direction was given and the method did not return false'
        );

        $this->assertTrue(
            $this->Robot->isFacingValid('NORTH'),
            'A valid facing direction was given and the method did not return true'
        );
    }

    public function testSettingAndGettingThePosition() {

        $this->Robot->setPosition(5,5);

        $this->assertInternalType('array', $this->Robot->getPosition(), 'The data retured was not an array');

        $this->assertArrayHasKey('x', $this->Robot->getPosition(), 'The getPosition array does not have a valid [x] key');
        $this->assertArrayHasKey('y', $this->Robot->getPosition(), 'The getPosition array does not have a valid [y] key');

        $this->assertEquals(['x'=>5,'y'=>5], $this->Robot->getPosition(), 'After setting the values the data returned did not match what was expected');
    }

    public function testSettingAndGettingTheFacingDirection() {
        $this->Robot->setFacing('NORTH');

        $this->assertInternalType('string', $this->Robot->getFacing(), 'The data retured was not string');

        $this->assertEquals('NORTH', $this->Robot->getFacing(), 'After setting the value the data returned did not match what was expected');
    }

    public function testMethod_isInstructionValid() {

        $this->assertFalse($this->Robot->isInstructionValid('INVALID'), 'An invalid instruction was sent and it did not fail the validation');

        $this->assertInternalType('string', $this->Robot->isInstructionValid('PLACE'), 'A valid instruction was sent and it did not return a string value');
    }

    public function testMethod_isPlacementValid() {

        $this->assertFalse($this->Robot->isPlacementValid('0,0,NORTH'), 'An invalid value was sent and it did not fail the validation');

        $this->assertFalse($this->Robot->isPlacementValid('0,NORTH,0'), 'An invalid value was sent and it did not fail the validation');

        $this->assertFalse($this->Robot->isPlacementValid('0,0,NORTH PLACE'), 'An invalid value was sent and it did not fail the validation');

        $this->assertInternalType('array', $this->Robot->isPlacementValid('PLACE 0,0,NORTH'), 'The data retured was not an array');

    }
}