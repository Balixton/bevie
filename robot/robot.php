<?php

include('src/RobotClass.php');

$Robot = new \src\RobotClass();

// On startup if we do not receive a file parameter we will assume instructions will be passed on the command line
if ($argc > 1 && $argv[1] != '') {
    // We will assume the parameter passed is a filepath and name that contains instructions
    $inputFile = $argv[1];
    echo "File [{$inputFile}] ";
    if (file_exists($inputFile)) {
        echo 'is valid ... processing.'.PHP_EOL;

        // We have a file, we don't know what the contents are, but lets read them all in and send off to be processed.
        $instructions = file($inputFile, FILE_IGNORE_NEW_LINES);
        if (is_array($instructions) && count($instructions) > 0) {
            foreach($instructions as $instruction) {
                $response = $Robot->processInstruction($instruction);
                echo ((!$response) ? "Instruction: [{$instruction}] ... failed" : $response).PHP_EOL;
            }
        } else {
            echo 'File empty, no instructions found.'.PHP_EOL;
        }

    } else {
        echo 'is not valid. Please pass the full path and file name.'.PHP_EOL;
    }

} else {
    echo "Awaiting instructions ... CTRL+C or type 'exit'".PHP_EOL;

    $stdin = fopen('php://stdin', 'r');
    $exit = false;

    while (!$exit) {
        $instruction = trim(fgets($stdin));

        if ($instruction != '' && $instruction != 'exit') {
            $response = $Robot->processInstruction($instruction);
            echo ((!$response) ? "Instruction: [{$instruction}] ... failed" : $response).PHP_EOL;
        } elseif ($instruction == 'exit')
            $exit = true;
    }
}