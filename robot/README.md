# Bevie Robot Test

## Setup
- No setup should be required
- I ran this project on Ubuntu 14.04

``` bash
git clone git clone https://Balixton@bitbucket.org/Balixton/bevie.git bevie-weather

cd bevie-weather/robot

```

## Notes
- The project will run on PHP 5.6 and 7
- All comments and assumptions are commented throughout the code
- Three test input files have been supplied

## To run
- On the command line, in the projects root directory, you can run 'php ./robot.php'
- The prompt will await your input instructions.
- You can supply an input file by passing it as a parameter, eg; 'php ./robot.php test_instructions1.txt'

## Unit testing
- To run the unit tests, in the projects root directory run './vendor/bin/phpunit'